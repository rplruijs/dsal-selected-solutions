package nl.hhs.dsal.week5.opgave_merge_sort;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MergeSortTest {
    @Test
    public void merge_sort_must_sort_a_string_lexographically_correctly() {


        String[] inputTest = {"Aap", "Noot", "Mies"};
        MergeSorter.sort(inputTest);

        String[] expected = {"Aap", "Mies", "Noot"};

        assertArrayEquals(expected, inputTest);

    }



}