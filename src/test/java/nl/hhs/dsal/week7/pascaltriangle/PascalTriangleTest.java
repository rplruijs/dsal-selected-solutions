package nl.hhs.dsal.week7.pascaltriangle;

import org.junit.Test;

public class PascalTriangleTest {

    @Test
    public void pascal_0_2_equels_1(){
        assert(PascalTriangle.pascal(0, 2) == 1);
    }

    @Test
    public void pascal_1_2_equels_2(){
        assert(PascalTriangle.pascal(1, 2) == 2);
    }

    @Test
    public void pascal_1_3_equels_3(){
        assert(PascalTriangle.pascal(1, 3) == 3);
    }


}