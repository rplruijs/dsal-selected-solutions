package nl.hhs.dsal.week1;

import org.junit.Test;

import static org.junit.Assert.*;

public class PE15dot3Test {
    @Test
    public void reverseSentence(){

        String testString = "Mary had a little lamb. Its fleece was white as snow.";

        String expectedResult = "Lamb little a had mary. Snow as white was fleece its.";

        String actualResult = PE15dot3.reverseSentence(testString);

        assertEquals(expectedResult.length(), actualResult.length());

        assertEquals(expectedResult, actualResult);
    }

}