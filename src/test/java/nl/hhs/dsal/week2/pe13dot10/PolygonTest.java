package nl.hhs.dsal.week2.pe13dot10;

import org.junit.Test;

import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class PolygonTest {
    @Test
    public void getArea(){

            List<Point2D.Double> corners = Arrays.asList(

            new Point2D.Double(0,0),
            new Point2D.Double(3,3),
            new Point2D.Double(0,6),
            new Point2D.Double(3,-3)

        );

        IPolygon polygon = new Polygon();
        for(Point2D.Double p : corners) polygon.add(p);

        double actualResult = polygon.getArea();
        double expectedResult = 18.0;

        assertEquals(expectedResult,actualResult, 0);

    }

}