package nl.hhs.dsal.week2;

import org.junit.Test;

import static org.junit.Assert.*;

public class P13dot11Test {
    @Test
    public void squareRoot() throws Exception {

        double actualValue1 = P13dot11.squareRoot(9);
        double expectedValue1 = 3d;


        double actualValue2 = P13dot11.squareRoot(100);
        double expectedValue2 = 10;


        assertEquals(actualValue1, expectedValue1, P13dot11.TRESSHOLD);
        assertEquals(actualValue2, expectedValue2, P13dot11.TRESSHOLD);


    }

}