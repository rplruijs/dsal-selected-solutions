package nl.hhs.dsal.week2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PE13dot6Test {

    @Test
    public void testReverse(){
        boolean resultaat1 = PE13dot6.find("Banaan","aan"); // Zou true moeten zijn.
        boolean resultaat2 = PE13dot6.find("Banaan","appel"); // Zou false moeten zijn.

        assert(resultaat1);
        assert(!resultaat2);

    }
}
