package nl.hhs.dsal.week2;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class PE13dot12Test {

    @Test
    public void subStrings(){
        String testCase1 = "rum";

        Set<String> actualResult1 = new HashSet<String>(PE13dot12.subStrings(testCase1));
        Set<String> expectedResult1 = new HashSet<String>(Arrays.asList("r", "ru", "rum", "u", "um", "m", ""));

        assertEquals(expectedResult1, actualResult1);

    }

}