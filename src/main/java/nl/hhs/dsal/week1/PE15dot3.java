package nl.hhs.dsal.week1;

import java.util.Scanner;
import java.util.Stack;

public class PE15dot3 {

    public static String reverseSentence(String inp) {
        StringBuilder builder = new StringBuilder();

        Scanner reader = new Scanner(inp);

        Stack<String> reverser =  new Stack<String>();

        while(reader.hasNext()){

            String currentWord = reader.next();
            if(currentWord.endsWith(".")){
                reverser.push(currentWord.substring(0, currentWord.length() -1).toLowerCase()); // Get rid of the .
                builder.append(printSentenceFromStack(reverser));
            }else{
                reverser.push(currentWord.toLowerCase());
            }
        }

        return builder.toString().trim();

    }

    private static String printSentenceFromStack(Stack<String> reverser) {


        StringBuilder builder = new StringBuilder();
        builder.append(capitalizeFirstCharacterOfWord(reverser.pop()));

        builder.append(" ");

        while(!reverser.isEmpty()){
            builder.append(reverser.pop());
            if(reverser.size()>0){
                builder.append(" ");
            }
        }

        builder.append(".");
        builder.append(" ");

        return builder.toString();
    }

    private static String capitalizeFirstCharacterOfWord(String inp){
        String firstChar = "" + inp.charAt(0);
        String restString = inp.substring(1);

        return firstChar.toUpperCase() + restString;
    }
}
