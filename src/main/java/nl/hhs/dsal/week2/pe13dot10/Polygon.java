package nl.hhs.dsal.week2.pe13dot10;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Polygon implements IPolygon
{
    private List<Point2D.Double> corners;

    public Polygon(){
        this.corners = new ArrayList<Point2D.Double>();
    }

    public void add(Point2D.Double p) {
        corners.add(p);
    }

    public double getArea() {

        if(corners.size() < 3){
            throw new IllegalStateException("Een polygoon moet minimaal 3 hoekpunten hebben.");
        }


        Point2D p1 = corners.get(0);
        Point2D p2 = corners.get(1);
        Point2D p3 = corners.get(2);

        double t1 = p1.getX() *  p2.getY();
        double t2 = p2.getX() *  p3.getY();
        double t3 = p3.getX() *  p1.getY();

        double t4 = p1.getY() *  p2.getX();
        double t5 = p2.getY() *  p3.getX();
        double t6 = p3.getY() *  p1.getX();

        double area = Math.abs(t1 + t2 + t3 -t4 - t5 - t6)/2;

        corners.remove(p2);

        if(corners.size() >= 3){
            Polygon remainder = new Polygon();



            for(Point2D.Double p : corners){
                remainder.add(p);
            }

            return area + remainder.getArea();
        }else{
            return area;
        }


    }
}
