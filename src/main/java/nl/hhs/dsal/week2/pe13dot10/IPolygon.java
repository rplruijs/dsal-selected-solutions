package nl.hhs.dsal.week2.pe13dot10;

import java.awt.geom.Point2D;

public interface IPolygon {

    public void add(Point2D.Double p);
    public double getArea();

}
