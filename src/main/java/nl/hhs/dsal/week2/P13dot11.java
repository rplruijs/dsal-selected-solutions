package nl.hhs.dsal.week2;

public class P13dot11 {


    final static double TRESSHOLD = 0.05;

    public static double squareRoot(double x){
        if(x <= 0) throw new IllegalArgumentException("Provide a positive number");
        return squareRoot(x, 1232);
    }

    private static double squareRoot(double x, double guess){
        if(Math.abs(guess * guess - x) < TRESSHOLD){
            return guess;
        }else{
            double newGuess = (guess + x / guess) / 2;
            return squareRoot(x, newGuess);
        }
    }
}
