package nl.hhs.dsal.week2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PE13dot12 {

    public static List<String> subStrings(String s){

        if(s.length() == 0){
            return new ArrayList<String>();
        };

        if(s.length() == 1){
            return Arrays.asList(s);
        }

        List<String> subResult = new ArrayList<String>();

        for(int i=0; i< s.length() + 1; i++){
            String iter = s.substring(0, i);
            subResult.add(iter);
        }


        String recursiveRestString = s.substring(1, s.length());

        List<String> restSubs = subStrings(recursiveRestString);
        subResult.addAll(restSubs);


        return subResult;

    }
}
