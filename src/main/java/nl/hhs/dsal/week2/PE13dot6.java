package nl.hhs.dsal.week2;

public class PE13dot6 {

    public static boolean find(String text, String str){

        //Basisgeval
        if(str.length() > text.length()){return false;}

        //Indien str exact overeenkomt met het 'begin' vam text, dan hebben we de string gevonden en kunnen we true retourneren
        if(text.startsWith(str)) {return true;}

        //We hebben nog de String str nog niet gevonden, wellicht in de 'staart' van de text.
        //We gaan in recursie
        String restString = text.substring(1);
        return find(restString, str);

    }
}
