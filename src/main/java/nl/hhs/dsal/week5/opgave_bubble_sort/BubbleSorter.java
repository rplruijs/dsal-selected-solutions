package nl.hhs.dsal.week5.opgave_bubble_sort;

public class BubbleSorter {

    public static <T extends Comparable> void bubbleSort(T[] inp ){

        boolean notSortedYet = true;
        int j = 0;

        while(notSortedYet){
            notSortedYet = false;
            j++;
            for(int i=0; i<inp.length-j; i++){

                T current = inp[i];
                T next = inp[i+1];

                if(current.compareTo(next) > 0){
                    swap(inp, i, i+1);
                    notSortedYet =  true;
                }
            }

        }
    }


    private static<T extends Comparable> void swap (T[] inp, int i, int j){
        T temp = inp[i];
        inp[i] = inp[j];
        inp[j] = temp;
    }
}

