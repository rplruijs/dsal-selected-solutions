package nl.hhs.dsal.week5.opgaven_remove_duplicates;

import java.util.Arrays;

public class RemoveDuplicates {

    public static int[] removeDuplicates(int[] inp){

        Arrays.sort(inp); // O(nlog(n))

        for(int i=0; i<inp.length -1; i++){

            if(inp[i] == inp[i+1]){
               inp= removeElementAtIndex(i, inp);
            }
        }

        return inp;
     }



    public static int[] removeElementAtIndex(int index, int[] inp){
        if(index >= inp.length | index < 0) throw new IllegalArgumentException("Unknown index");

        int[] retArr = new int[inp.length -1];

        for(int i = 0; i < inp.length-1; i++){
            if(i < index) retArr[i] = inp[i];
            if(i >= index) retArr[i] = inp[i+1];
        }

        return retArr;
    }
}
