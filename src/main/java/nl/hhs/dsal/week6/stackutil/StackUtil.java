package nl.hhs.dsal.week6.stackutil;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class StackUtil {

    public static<T> List<T> getOndersteElementen(Stack<T> stack, int n){

        int initialStackSize = stack.size();
        int nrOfPops = initialStackSize - n;
        List<T> returner = new ArrayList();

        for(int i=0; i < initialStackSize; i++){
            if(i<nrOfPops) {
                stack.pop();
            }
            else {
                returner.add(stack.pop());
            }
        }

        return returner;

    }
}
