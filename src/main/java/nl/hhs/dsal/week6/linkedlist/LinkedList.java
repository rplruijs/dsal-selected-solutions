package nl.hhs.dsal.week6.linkedlist;

import java.util.NoSuchElementException;

/**
 * A linked list is a sequence of nodes with efficient
 * element insertion and removal. This class
 * contains a subset of the methods of the standard
 * java.util.LinkedList class.
 */
public class LinkedList {
   private Node first;

   /**
    * Constructs an empty linked list.
    */
   public LinkedList() {
      first = null;
   }

   /**
    * Returns the first element in the linked list.
    *
    * @return the first element in the linked list
    */
   public Object getFirst() {
      if (first == null) {
         throw new NoSuchElementException();
      }
      return first.data;
   }

   /**
    * Removes the first element in the linked list.
    *
    * @return the removed element
    */
   public Object removeFirst() {
      if (first == null) {
         throw new NoSuchElementException();
      }
      Object element = first.data;
      first = first.next;
      return element;
   }

   /**
    * Adds an element to the front of the linked list.
    *
    * @param element the element to add
    */
   public void addFirst(Object element) {
      Node newNode = new Node();
      newNode.data = element;
      newNode.next = first;
      first = newNode;
   }


   /**
    * Opgave 2
    *
    */

   /**
    * Gets the node at the given index.
    *
    * @param n The index of the object to get.
    * @return The node at the given object, or null if it doesn't exist.
    */

   public Node getNode(int n) {
      if (n == 0) return first;
      int i = 0;
      Node current = first;
      while (current.next != null) {
         current = current.next;
         i++;
         if (i == n) return current;
      }
      return null;
   }

   /**
    *
    * Opgave 3
    */

   /**
    * Gets the node at the given index.
    *
    * @param n The index of the object to get.
    * @return The node at the given object, or null if it doesn't exist.
    */

   public Node getNodeRec(int n) {

      if (n < 0) return null;
      return getNodeRec(first, n);
   }

   private Node getNodeRec(Node current, int distance) {
      if ((distance <= 0) || (current == null)) {
         return current;
      }
      return getNodeRec(current.next, distance - 1);
   }


   /**
    * Opgave 4
    */



   /**
    * Sets the object at the given index.
    *
    * @param n          The index of the object to be set.
    * @param newElement The object to set.
    */
   public void set(int n, Object newElement) {
      Node node = getNode(n);

      if (node == null) throw new IllegalArgumentException("Unknown index");

      node.data = newElement;
   }


   /**
    * Opgave 5
    */

   /**
    * Reverses all elements in a linked list.
    */
   public void reverse() {

      if (first == null) return;

      Node previous = first;
      Node current = first.next;
      first.next = null;
      while (current != null) {
         Node next = current.next;
         current.next = previous;
         previous = current;
         current = next;
      }
      first = previous;

   }










   /**
      Returns an iterator for iterating through this list.
      @return an iterator for iterating through this list
   */
   public ListIterator listIterator()
   {  
      return new LinkedListIterator();
   }
   
   class Node
   {  
      public Object data;
      public Node next;
   }

   class LinkedListIterator implements ListIterator
   {  
      private Node position;
      private Node previous;
      private boolean isAfterNext;

      /**
         Constructs an iterator that points to the front
         of the linked list.
      */
      public LinkedListIterator()
      {  
         position = null;
         previous = null;
         isAfterNext = false;
      }
      
      /**
         Moves the iterator past the next element.
         @return the traversed element
      */
      public Object next()
      {  
         if (!hasNext()) { throw new NoSuchElementException(); }
         previous = position; // Remember for remove
         isAfterNext = true;

         if (position == null)
         {
            position = first;
         }
         else
         {
            position = position.next;
         }

         return position.data;
      }
      
      /**
         Tests if there is an element after the iterator position.
         @return true if there is an element after the iterator position
      */
      public boolean hasNext()
      {  
         if (position == null)
         {
            return first != null;
         }
         else
         {
            return position.next != null;
         }
      }
      
      /**
         Adds an element before the iterator position
         and moves the iterator past the inserted element.
         @param element the element to add
      */
      public void add(Object element)
      {  
         if (position == null)
         {
            addFirst(element);
            position = first;
         }
         else
         {  
            Node newNode = new Node();
            newNode.data = element;
            newNode.next = position.next;
            position.next = newNode;
            position = newNode;
         }

         isAfterNext = false;
      }
      
      /**
         Removes the last traversed element. This method may
         only be called after a call to the next() method.
      */
      public void remove()
      {  
         if (!isAfterNext) { throw new IllegalStateException(); }

         if (position == first)
         {
            removeFirst();
         }
         else 
         {  
            previous.next = position.next;
         }
         position = previous;
         isAfterNext = false;
      }

      /**
         Sets the last traversed element to a different value. 
         @param element the element to set
      */
      public void set(Object element)
      {
         if (!isAfterNext) { throw new IllegalStateException(); }
         position.data = element;
      }
   }
}
