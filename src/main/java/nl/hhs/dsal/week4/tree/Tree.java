

package nl.hhs.dsal.week4.tree;

/**
 *
 * @author remcoruijsenaars
 */
public class Tree {
    
    private Node root;
    
    public Tree(Node root){
        this.root = root;
    }
    
    public int size(){

        if(root == null){
            return 0;
        }else{
            return root.getSize();
        }
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }
    
    public void addSubTree(Tree subTree){
        root.getChildren().add(subTree.getRoot());
    }
    
    public void addNodeToRoot(Node node){
        root.getChildren().add(node);
    }

    /**
     *
     *       A
     *    / | | \
     *   B  C D  E
     *  /
     *  F
     */

    /**
     * Opgave  2 (P17.3)
     * @return het aantal bladeren van een boom.
     */
    public int numberOfLeaves(){
        return numberOfLeaves(root);
    }

    private int numberOfLeaves(Node n){

        if(n.isLeaf()) return 1;
        else{
            int retNr = 0;
            for( Node nIter : n.getChildren()){
                retNr = retNr + numberOfLeaves(nIter);
            }
            return retNr;
        }
    }
    
    @Override
    public String toString(){
        return root.toString();
    }

}
