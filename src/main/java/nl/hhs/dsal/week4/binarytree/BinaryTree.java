package nl.hhs.dsal.week4.binarytree;

/**
 *
 * @author remcoruijsenaars
 */
public class BinaryTree {
    
    private Node root;
    

    public BinaryTree(Node node){
        root = node;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }


    public int getHeight(){
        return height(root);
    }


    private int height(Node n){
        if(n==null){
            return 0;
        }else{
            return 1 + Math.max(height(n.getLeft()), height(n.getRight()));
        }
    }

    /**
     * Opgave 3 (P17.4)
     * @return het aantal knopen dat 1 kind heeft.
     */
    public int countNodesWithOneChild(){
        return countNodesWithOneChild(root);
    }

    private int countNodesWithOneChild(Node node){


//        if (node == null) return 0;
//
//        int current = 0;
//
//        if(node.oneChild()) {
//            current = 1;
//        }
//
//        int aantalL = countNodesWithOneChild(node.getLeft());
//        int aantalR = countNodesWithOneChild(node.getRight());
//
//        return current + aantalL + aantalR;

        return (node.oneChild()? 1 : 0) +
                countNodesWithOneChild(node.getRight()) +
                countNodesWithOneChild(node.getLeft());

    }

    /**
     * opgave 5
     * Implementeer inorderTraversal en preOrderTraversal
     * @param v is een object dat gebruikt moet worden om de huidige node te bezoeken.
     */

    public void inorderTraversal(Visitor v){
        inOrderTraversal(root, v);

    }

    private void inOrderTraversal(Node node, Visitor v){
        if(node == null) return;

        inOrderTraversal(node.getLeft(), v);
        v.visit(node);
        inOrderTraversal(node.getRight(), v);
    }


    public void preOrderTraversal(Visitor v){
        preOrderTraversal(root, v);
    }

    private void preOrderTraversal(Node node, Visitor v){
        if(node == null) return;

        v.visit(node);
        preOrderTraversal(node.getLeft(), v);
        preOrderTraversal(node.getRight(), v);
    }

    /**
     * Opgave 6 (EXTRA)
     * @return true waneer de boom gebalanceerd is, false indien dit niet het geval is.
     *
     * De recursieve definitie van een gebalanceerd boom luidt als gevolgd.
     * Het verschil tussen de hoogte van de linker subboom en rechter subboom is maximaal 1.
     *  De linkerboom is gebalanceerd
     *  De rechterboom is gebalanceerd
     *
     */
    public boolean isBalanced(){
        return isBalanced(root);
    }

    private boolean isBalanced(Node node){

        if(node==null || node.isLeaf() ) return true;

        int leftHeight = height(node.getLeft());
        int rightHeight = height(node.getRight());

        boolean currentIsBalanced = Math.abs(leftHeight - rightHeight) <= 1;

        return currentIsBalanced && isBalanced(node.getLeft()) && isBalanced(node.getRight());
    }

}
