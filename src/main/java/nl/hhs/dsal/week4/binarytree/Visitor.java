package nl.hhs.dsal.week4.binarytree;

public interface Visitor {

    void visit(Node o);
}
