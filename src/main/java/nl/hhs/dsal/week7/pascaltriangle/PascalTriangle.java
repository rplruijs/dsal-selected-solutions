package nl.hhs.dsal.week7.pascaltriangle;

public class PascalTriangle {



    public static int pascal(int c, int r){
        if(c==0) return 1;
        else if(c==r) return 1;
        else if(c<0 | r<0) return 0;
        else return pascal(c-1, r-1) + pascal(c, r-1);
    }



}
