package nl.hhs.dsal.week3.pe13dot23;

import java.util.Scanner;

/**
   This class solves the eight queens problem using backtracking.
*/
public class NQueens
{
   public static void main(String[] args)
   {

      Scanner reader = new Scanner(System.in);
      System.out.println("Provide a positive number (integer) for n queens problem");
      int n = Integer.parseInt(reader.nextLine());
      solve(new PartialSolution(0, n));
   }

   /**
      Prints all solutions to the problem that can be extended from 
      a given partial solution.
      @param sol the partial solution
   */
   public static void solve(PartialSolution sol)
   {
      int exam = sol.examine();
      if (exam == PartialSolution.ACCEPT) 
      { 
         System.out.println(sol); 
      }
      else if (exam != PartialSolution.ABANDON)
      {
         for (PartialSolution p : sol.extend())
         {
            solve(p);
         }
      }
   }
}

