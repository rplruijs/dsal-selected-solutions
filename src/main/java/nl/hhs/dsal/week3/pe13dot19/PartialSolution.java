package nl.hhs.dsal.week3.pe13dot19;

public interface PartialSolution {

    public static final int ACCEPT = 1;
    public static final int ABANDON = 2;
    public static final int CONTINUE = 3;


    public int examine();
    public PartialSolution[] extend();

}
